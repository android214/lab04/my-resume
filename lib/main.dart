import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    Widget headerSection = Container(
      padding: EdgeInsets.all(20),
      child: Row(
        children: [
          Expanded(
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 8),
                child: Text('WATCHARA IAMIMSART',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text('COMPUTER SCIENCE',
                  style: TextStyle(
                    fontSize: 20,
                    color: Colors.grey[500]
                  ),
                ),
              ),
              Container(
                padding: const EdgeInsets.only(top: 20),
                child: Image.asset('images/Profile.jpg',
                  width: 500,
                  height: 350,
                  fit: BoxFit.fitHeight
                ),
              ),
              // Image.asset('images/Profile.jpg',
              //   width: 500,
              //   height: 350,
              //   fit: BoxFit.fitHeight
              // ),
            ],
          )),
        ],
      ),
    );

    Widget aboutMeSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildTopic('About Me'),
          Container(
            child: Table(
              children: [
                _buildData('Nickname:', 'Blue'),
                _buildData('Gender:', 'Male'),
                _buildData('Age:', '22 years old'),
                _buildData('Birthday:', '17 Nov 1998'),
              ],
            )
          )
        ],
      ),
    );

    Widget contactSection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Container(
            child: Column(
              children: [
                _buildTopic('Contact'),
                _buildContact(Icons.call, 'Tel:', '083-926-0891'),
                _buildContact(Icons.email, 'Email:', '60160131@go.buu.ac.th'),
              ],
            )
          )
        ],
      ),
    );

    Widget skillSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildTopic('Skill'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildskill('images/python.png', 'Python'),
              _buildskill('images/js.png', 'Java Script'),
              _buildskill('images/html5.png', 'HTML'),
              _buildskill('images/css.png', 'CSS'),
              _buildskill('images/vuejs.png', 'VueJS'),
              _buildskill('images/swift.png', 'Swift'),
              _buildskill('images/flutter.png', 'Flutter'),
            ],
          )
        ],
      ),
    );

    Widget educationSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          _buildTopic('Education'),
          Container(
            child: Table(
              children: [
                _buildEducate('2017 - present', 'Bachelor of Science (Computer Science), Faculty of Informatics', 'Burapha University, Chonburi'),
                _buildEducate('2011 - 2016', 'Science, Mathematics and Technology Special Class', 'Ayutthaya Wittayalai School, Ayutthaya'),
              ],
            )
          )
        ],
      ),
    );


    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Resume'),
          backgroundColor: Colors.lightBlue[100]
        ),
        body: ListView(
          children: [
            headerSection,
            aboutMeSection,
            contactSection,
            skillSection,
            educationSection
          ],
        ),
        ),

    );
  }
}

Container _buildTopic(String label){
  return Container(
    padding: EdgeInsets.only(left: 10, top: 40, bottom: 30),
    child: Text(label,
      style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 20
        ),
    ),
  );
}

TableRow _buildData(String topic, String detail){
  return TableRow(
    children: [
      Container(
        padding: EdgeInsets.only(left: 90, top: 8, bottom: 8),
        child: Text(topic,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
      Container(
        padding: EdgeInsets.only(left: 30, top: 8, bottom: 8),
        child: Text(detail,
          style: TextStyle(
            fontSize: 17
          ),
        ),
      )
    ],
  );
}

Row _buildContact(IconData icon, String topic, String detail){
  return Row(
    children: [
      Container(
        padding: EdgeInsets.only(left: 10),
        child: Icon(icon),
      ),
      Container(
        padding: EdgeInsets.all(8),
        child: Text(topic,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
      Container(
        padding: EdgeInsets.only(left: 5),
        child: Text(detail,
          style: TextStyle(
            fontSize: 17
          ),
        ),
      )
    ],
  );
}

Column _buildskill(String path, String label){
  return Column(
    children: [
      Image.asset(path, width: 50, height: 50,),
      Container(
        padding: EdgeInsets.only(top: 15),
        child: Text(label,
          style: TextStyle(
            fontSize: 15,
            fontWeight: FontWeight.bold
          ),
        ),
      )
    ],
  );
}

TableRow _buildEducate(String year, String faculty, String institution){
  return TableRow(
    children: [
      Container(
        padding: EdgeInsets.only(top: 10, left: 20),
        child: Text(year,
          style: TextStyle(
            fontSize: 17,
            fontWeight: FontWeight.bold
          ),
        ),
      ),
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.only(top:10, bottom: 10),
            child: Text(faculty,
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.bold
              ),
            ),
          ),
          Container(
            child: Text(institution,
              style: TextStyle(
                fontSize: 15,
              ),
            ),
          ),
        ],
      )
    ]
  );
}


